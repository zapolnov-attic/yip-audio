/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 The FriedCroc Team (admin@friedcroc.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __1cb4bcc3e2cbf1f2178e6ba76bff237c__
#define __1cb4bcc3e2cbf1f2178e6ba76bff237c__

#include <functional>

namespace Audio
{
	/**
	 * Starts recording audio.
	 * @param callback Callback to call for input data. Callback accepts three arguments: (1) pointer
	 * to recorded sound samples; (2) number of samples; and (3) number of channels. Length of the array
	 * of samples is equal to number of samples multiplied by number of channels.
	 */
	void startRecord(std::function<void(const float *, size_t, size_t)> callback);

	/** Stops recording audio. */
	void stopRecord();
}

#endif
